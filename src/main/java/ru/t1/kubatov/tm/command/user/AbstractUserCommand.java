package ru.t1.kubatov.tm.command.user;

import ru.t1.kubatov.tm.api.service.IAuthService;
import ru.t1.kubatov.tm.api.service.IUserService;
import ru.t1.kubatov.tm.command.AbstractCommand;
import ru.t1.kubatov.tm.exception.entity.UserNotFoundException;
import ru.t1.kubatov.tm.model.User;

public abstract class AbstractUserCommand extends AbstractCommand {

    protected IUserService getUserService() {
        return getServiceLocator().getUserService();
    }

    protected IAuthService getAuthService() {
        return getServiceLocator().getAuthService();
    }

    @Override
    public String getArgument() {
        return null;
    }

    protected void showUser(final User user) {
        if (user == null) throw new UserNotFoundException();
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
    }

}

package ru.t1.kubatov.tm.command.system;

import ru.t1.kubatov.tm.api.service.IPropertyService;

public class ApplicationDeveloperInfoCommand extends AbstractSystemCommand {

    public final static String DESCRIPTION = "Display application info.";

    public final static String NAME = "info";

    public final static String ARGUMENT = "-i";

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        IPropertyService propertyService = getPropertyService();

        System.out.println("[APPLICATION]");
        System.out.println("NAME: " + propertyService.getApplicationName());
        System.out.println();

        System.out.println("[DEVELOPER INFORMATION]");
        System.out.println(propertyService.getAuthorName());
        System.out.println(propertyService.getAuthorEmail());
        System.out.println();

        System.out.println("[GIT]");
        System.out.println("BRANCH: " + propertyService.getGitBranch());
        System.out.println("COMMIT ID: " + propertyService.getGitCommitId());
        System.out.println("COMMIT MESSAGE: " + propertyService.getGitCommitMessage());
        System.out.println("COMMIT TIME: " + propertyService.getGitCommitTime());
        System.out.println("COMMITTER NAME: " + propertyService.getGitCommitterName());
        System.out.println("COMMITTER EMAIL: " + propertyService.getGitCommitterEmail());
    }

}

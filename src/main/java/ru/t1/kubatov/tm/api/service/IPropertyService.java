package ru.t1.kubatov.tm.api.service;

public interface IPropertyService extends ISaltProvider {

    String getApplicationVersion();

    String getAuthorEmail();

    String getAuthorName();

    String getApplicationName();

    String getApplicationConfig();

    String getGitBranch();

    String getGitCommitId();

    String getGitCommitMessage();

    String getGitCommitTime();

    String getGitCommitterName();

    String getGitCommitterEmail();

}

package ru.t1.kubatov.tm.api.service;

import ru.t1.kubatov.tm.api.repository.IProjectRepository;
import ru.t1.kubatov.tm.enumerated.Sort;
import ru.t1.kubatov.tm.enumerated.Status;
import ru.t1.kubatov.tm.model.Project;

import java.util.List;

public interface IProjectService extends IProjectRepository {

    Project updateById(String userId, String id, String name, String description);

    Project updateByIndex(String userId, Integer index, String name, String description);

    Project changeProjectStatusById(String userId, String id, Status status);

    Project changeProjectStatusByIndex(String userId, Integer index, Status status);

    List<Project> findAll(String userId, Sort sort);

}

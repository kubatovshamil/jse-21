package ru.t1.kubatov.tm.api.service;

public interface ISaltProvider {

    Integer getPasswordIteration();

    String getPasswordSecret();

}

package ru.t1.kubatov.tm.api.model;

import ru.t1.kubatov.tm.enumerated.Role;

public interface ICommand {

    String getArgument();

    String getDescription();

    String getName();

    void execute();

    Role[] getRoles();

}

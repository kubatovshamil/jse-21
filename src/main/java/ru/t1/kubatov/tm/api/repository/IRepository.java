package ru.t1.kubatov.tm.api.repository;

import ru.t1.kubatov.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    void deleteAll();

    void deleteAll(Collection<M> collection);

    List<M> findAll();

    List<M> findAll(Comparator<M> comparator);

    M add(M model);

    boolean existsById(String id);

    M findById(String id);

    M findByIndex(Integer index);

    M delete(M model);

    M deleteById(String id);

    M deleteByIndex(Integer index);

    int getSize();

}

package ru.t1.kubatov.tm.api.model;

import ru.t1.kubatov.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
